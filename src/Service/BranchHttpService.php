<?php

namespace App\Service;

use App\Model\Transportation\CommonBranch;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Contracts\HttpClient\Exception\HttpExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class BranchHttpService
{

    /** @var HttpClientInterface $client */
    private $client;

    /** @var BranchBuilder $branchBuilder */
    private $branchBuilder;

    public function __construct(HttpClientInterface $client, BranchBuilder $branchBuilder)
    {
        $this->client = $client;
        $this->branchBuilder = $branchBuilder;
    }


    /**
     * @param string $url
     * @return array<CommonBranch>
     */
    public function fetchCollection(string $url): array
    {
        return $this->branchBuilder->buildBranches($this->getResponse($url), $url);
    }

    public function fetchItem(int $id, string $url): array
    {
        $collectionData = $this->getResponse($url);
        $itemKey = array_search($id, array_column($collectionData, 'id'));
        $item = $itemKey !== false ? $collectionData[$itemKey] : null;

        if (!$item) {
            throw new NotFoundHttpException("Item with id: {$id} was not found");
        }
        return $this->branchBuilder->buildBranches([$item], $url);
    }

    private function getResponse(string $url): array
    {
        try {
            $response = $this->client->request(
                Request::METHOD_GET,
                $url
            );
            return $response->toArray();
        } catch (HttpExceptionInterface | TransportExceptionInterface $e) {
            return [];
        }
    }
}
