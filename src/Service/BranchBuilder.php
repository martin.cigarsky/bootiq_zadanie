<?php

declare(strict_types=1);

namespace App\Service;

use App\Model\Transportation\BranchModel;
use App\Model\Transportation\BusinessHourModel;
use App\Model\Transportation\CommonBranch;
use App\Model\Transportation\Coordinates;
use App\Model\Transportation\ReadableDay;

class BranchBuilder
{

    private const ULOZENKA_URL = 'https://www.ulozenka.cz/gmap';

    /**
     * @param array $data
     * @param string $url
     * @return array<CommonBranch>
     */
    public function buildBranches(array $data, string $url): array
    {
        switch ($url) {
            case self::ULOZENKA_URL:
                return $this->buildUlozenkaBranches($data);
            default:
                return $data;
        }
    }

    /**
     * @param array $data
     * @return array<BranchModel>
     */
    public function buildUlozenkaBranches(array $data): array
    {
        $branches = [];
        foreach ($data as $item) {
            $branches[] = (new BranchModel())
                ->setInternalId((string)$item['id'])
                ->setInternalName($item['name'])
                ->setLocation(new Coordinates($item['lat'], $item['lng']))
                ->setBusinessHours($this->buildBusinessHours($item['openingHours']))
                ->setAddress($item['name'])
                ->setWeb($item['odkaz'])
                ->setAnnouncement(implode(",", $item['announcements']))
                ->setShortcut($item['shortcut'])
                ->setActive((bool)$item['active'])
                ->setPicture($item['pic']);
        }

        return $branches;
    }

    private function buildBusinessHours(array $openingHours): array
    {
        $hours = [];
        foreach ($openingHours as $openingHour) {
            $hours[] = (new BusinessHourModel(
                ReadableDay::getReadableDay((string)$openingHour['day']),
                "{$openingHour['open']} - {$openingHour['close']}"
            ));
        }
        return $hours;
    }
}
