<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\BranchHttpService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\Transportation\BranchModel;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * @OA\Tag(name="Branches")
 * @Route("/branches")
 */
class BranchController extends AbstractController
{
    /** @var BranchHttpService $branchHttpService */
    private $branchHttpService;

    public function __construct(BranchHttpService $branchHttpService)
    {
        $this->branchHttpService = $branchHttpService;
    }

    /**
     * @Route("", methods={"GET"})
     * @OA\Parameter(
     *     name="url",
     *     in="query",
     *     required=true,
     *     description="Url of api",
     *     @OA\Schema(type="string", example="https://www.ulozenka.cz/gmap")
     * )
     * @OA\Response(
     *     response="200",
     *     description="List branches",
     *     @OA\JsonContent(
     *         type="array",
     *         @OA\Items(ref=@Model(type=BranchModel::class))
     *     )
     * )
     */
    public function listBranches(Request $request): JsonResponse
    {
        return $this->json($this->branchHttpService->fetchCollection($request->get('url')));
    }

    /**
     * @Route("/{id}", methods={"GET"})
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="ID of branch",
     *     @OA\Schema(type="integer", example=53492)
     * )
     * @OA\Parameter(
     *     name="url",
     *     in="query",
     *     required=true,
     *     description="Url of api",
     *     @OA\Schema(type="string", example="https://www.ulozenka.cz/gmap")
     * )
     * @OA\Response(
     *     response="200",
     *     description="Get branch",
     *     @Model(type=BranchModel::class)
     * )
     */
    public function getBranch(Request $request, int $id): JsonResponse
    {
        return $this->json($this->branchHttpService->fetchItem($id, $request->get('url')));
    }
}
