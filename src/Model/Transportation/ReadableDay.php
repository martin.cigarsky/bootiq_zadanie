<?php

declare(strict_types=1);

namespace App\Model\Transportation;

class ReadableDay
{
    public const MONDAY = 'Monday';
    public const TUESDAY = 'Tuesday';
    public const WEDNESDAY = 'Wednesday';
    public const THURSDAY = 'Thursday';
    public const FRIDAY = 'Friday';
    public const SATURDAY = 'Saturday';
    public const SUNDAY = 'Sunday';

    public static function getReadableDay(string $day): string
    {
        switch ($day) {
            case 1:
                return self::MONDAY;
            case 2:
                return self::TUESDAY;
            case 3:
                return self::WEDNESDAY;
            case 4:
                return self::THURSDAY;
            case 5:
                return self::FRIDAY;
            case 6:
                return self::SATURDAY;
            case 7:
                return self::SUNDAY;
            default:
                return '';
        }
    }
}
