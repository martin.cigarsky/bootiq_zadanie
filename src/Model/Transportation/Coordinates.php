<?php

declare(strict_types=1);

namespace App\Model\Transportation;

class Coordinates
{
    /** @var float */
    private $lat;

    /** @var float */
    private $lng;

    public function __construct(float $lat, float $lng)
    {
        $this->lat = $lat;
        $this->lng = $lng;
    }

    public function getLat(): float
    {
        return $this->lat;
    }

    public function getLng(): float
    {
        return $this->lng;
    }
}
