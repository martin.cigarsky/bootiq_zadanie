<?php

declare(strict_types=1);

namespace App\Model\Transportation;

class BranchModel implements CommonBranch
{
    /** @var string */
    private $internalId;

    /** @var string */
    private $internalName;

    /** @var Coordinates */
    private $location;

    /** @var array<BusinessHourModel> */
    private $businessHours;

    /** @var string */
    private $address;

    /** @var string */
    private $web;

    /** @var string */
    private $announcement;

    /** @var string */
    private $shortcut;

    /** @var bool */
    private $active;

    /** @var string */
    private $picture;

    public function getInternalId(): string
    {
        return $this->internalId;
    }

    public function setInternalId(string $internalId): self
    {
        $this->internalId = $internalId;
        return $this;
    }

    public function getInternalName(): string
    {
        return $this->internalName;
    }

    public function setInternalName(string $internalName): self
    {
        $this->internalName = $internalName;
        return $this;
    }

    public function getLocation(): Coordinates
    {
        return $this->location;
    }

    public function setLocation(Coordinates $location): self
    {
        $this->location = $location;
        return $this;
    }

    /** @return BusinessHourModel[] */
    public function getBusinessHours(): array
    {
        return $this->businessHours;
    }

    /** @param BusinessHourModel[] $businessHours */
    public function setBusinessHours(array $businessHours): self
    {
        $this->businessHours = $businessHours;
        return $this;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;
        return $this;
    }

    public function getWeb(): string
    {
        return $this->web;
    }

    public function setWeb(string $web): self
    {
        $this->web = $web;
        return $this;
    }

    public function getAnnouncement(): string
    {
        return $this->announcement;
    }

    public function setAnnouncement(string $announcement): self
    {
        $this->announcement = $announcement;
        return $this;
    }

    public function getShortcut(): string
    {
        return $this->shortcut;
    }

    public function setShortcut(string $shortcut): self
    {
        $this->shortcut = $shortcut;
        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;
        return $this;
    }

    public function getPicture(): string
    {
        return $this->picture;
    }

    public function setPicture(string $picture): self
    {
        $this->picture = $picture;
        return $this;
    }
}
